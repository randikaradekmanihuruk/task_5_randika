<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Home</name>
   <tag></tag>
   <elementGuidId>d5587eb1-2eca-471b-a3cf-5d482eb359ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li:nth-of-type(2) > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//a[@onclick=&quot;$('#menu-close').click();&quot;])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>f92a7bdd-4db1-41ca-862b-75f6323478cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>./</value>
      <webElementGuid>e33b00fc-cd23-4f79-996a-6a5018733589</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>$('#menu-close').click();</value>
      <webElementGuid>d39f3138-7010-4c03-bb7a-00a1466b0733</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Home</value>
      <webElementGuid>7bd6d0c8-6e67-45ad-a72a-cc1da05b0520</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sidebar-wrapper&quot;)/ul[@class=&quot;sidebar-nav&quot;]/li[2]/a[1]</value>
      <webElementGuid>f949426a-ff24-44de-bd61-07b36bd10cec</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//a[@onclick=&quot;$('#menu-close').click();&quot;])[2]</value>
      <webElementGuid>0d13b725-b9df-4bd7-ac8f-cf50900eb59e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//nav[@id='sidebar-wrapper']/ul/li[2]/a</value>
      <webElementGuid>1a49dbbd-18f5-48a9-b8a3-c18c3d667259</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Home')]</value>
      <webElementGuid>c6f209e0-dead-43b7-a7bc-040e65c37a04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CURA Healthcare'])[1]/following::a[1]</value>
      <webElementGuid>1eeadf7f-69b8-4ec8-b2cc-f4ea800bbc2d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='History'])[1]/preceding::a[1]</value>
      <webElementGuid>033badd0-9c60-4341-8bec-63f325bd0129</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profile'])[1]/preceding::a[2]</value>
      <webElementGuid>3abe792e-6a6c-4b7e-8306-5ceb0cda95e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Home']/parent::*</value>
      <webElementGuid>6b30a9cc-8e89-46e9-adb6-e4de19b51404</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, './')])[2]</value>
      <webElementGuid>9a667f55-a4bc-4a74-a02a-908ba7e27475</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a</value>
      <webElementGuid>b040b34c-2479-42ea-8a86-ff07ba5c65f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = './' and (text() = 'Home' or . = 'Home')]</value>
      <webElementGuid>c165e7c4-584b-41d3-95f0-863be4dabec2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
